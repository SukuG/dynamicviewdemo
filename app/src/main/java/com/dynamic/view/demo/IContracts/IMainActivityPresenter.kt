package com.dynamic.view.demo.IContracts

import android.content.Context

interface IMainActivityPresenter {
    fun loadLoginViewResponse(mainActivity: Context)
}