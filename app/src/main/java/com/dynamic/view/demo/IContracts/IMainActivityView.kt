package com.dynamic.view.demo.IContracts

import android.widget.LinearLayout
import com.dynamic.view.demo.dto.response.LoginViewResponse

interface IMainActivityView {
    fun setViewToParent(definedLayout: LinearLayout)
}