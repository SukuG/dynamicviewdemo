package com.dynamic.view.demo.dto.response

import com.google.gson.annotations.SerializedName

class LoginViewData {

    @SerializedName("layout_type")
    var layout_type: Int = -1

    @SerializedName("layout_id")
    var layout_id: String? = null

    @SerializedName("width")
    var width: Int? = -1

    @SerializedName("height")
    var height: Int? = -1

    @SerializedName("orientation")
    var orientation: Int? = -1

    @SerializedName("background_color")
    var background_color: String? = null

    @SerializedName("gravity")
    var gravity: Int? = -1

    @SerializedName("visibility")
    var visibility: Boolean = false

    @SerializedName("widgets")
    var widgets: List<LoginWidgetResponse>? = null
}