package com.dynamic.view.demo.dto.response

import com.google.gson.annotations.SerializedName

class LoginWidgetResponse {

    @SerializedName("type")
    var type: Int = -1

    @SerializedName("id")
    var id: String? = null

    @SerializedName("width")
    var width: Int? = -1

    @SerializedName("height")
    var height: Int? = -1

    @SerializedName("text")
    var text: String? = null

    @SerializedName("text_color")
    var text_color: String? = null

    @SerializedName("text_size")
    var text_size: Int? = -1

    @SerializedName("hint")
    var hint: String? = null

    @SerializedName("hint_color")
    var hint_color: String? = null

    @SerializedName("gravity")
    var gravity: Int? = -1

    @SerializedName("background_color")
    var background_color: String? = null

    @SerializedName("visibility")
    var visibility: Boolean = false

    @SerializedName("input_type")
    var input_type: Int = -1

    @SerializedName("isAllCaps")
    var isAllCaps: Boolean = false
}