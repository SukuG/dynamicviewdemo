package com.dynamic.view.demo.presenter

import android.content.Context
import android.text.InputType
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.dynamic.view.demo.IContracts.IMainActivityPresenter
import com.dynamic.view.demo.IContracts.IMainActivityView
import com.dynamic.view.demo.dto.response.LoginViewData
import com.dynamic.view.demo.dto.response.LoginViewResponse
import com.dynamic.view.demo.dto.response.LoginWidgetResponse
import com.dynamic.view.demo.util.CodeSnippet
import com.google.gson.Gson

class MainActivityPresenter(var iMainActivityView: IMainActivityView) : IMainActivityPresenter {


    var context: Context? = null// it'll change in later
    override fun loadLoginViewResponse(mainActivity: Context) {
        this.context = mainActivity
        val response = CodeSnippet.loadJSONFromAsset(mainActivity, "login_view.json")
        val convertedResponse =
            Gson().fromJson<LoginViewResponse>(
                response,
                LoginViewResponse::class.java
            )
        defineLayout(convertedResponse?.loginViews)
    }

    private fun defineLayout(loginViewData: LoginViewData?) {
        when (loginViewData?.layout_type) {
            CodeSnippet.ConstantValues.LAYOUT_LINEAR -> {
                val definedLayout = defineLinearLayout(loginViewData)
                val widgetsList = loginViewData.widgets
                if (widgetsList == null || widgetsList.isEmpty())
                    return
                defineWidgets(definedLayout, widgetsList)
                //add into parent layout
                iMainActivityView.setViewToParent(definedLayout)
            }
        }
    }

    private fun defineLinearLayout(loginViewData: LoginViewData?): LinearLayout { // in later, this method need to convert general method using T type class

        val linearLayout = LinearLayout(context)
        //let's define params
        val params = defineParams(loginViewData?.width, loginViewData?.height)
        linearLayout.layoutParams = params
        //let's define orientation
        val orientation = defineOrientation(loginViewData?.orientation)
        linearLayout.orientation = orientation

        // similar define padding and margin using params object.

        params.gravity = findGravity(loginViewData?.gravity)

        return linearLayout
    }

    private fun defineWidgets(
        definedLayout: LinearLayout,
        widgets: List<LoginWidgetResponse>
    ) {
        // in later, this method need to convert general method using T type class
        for (i in widgets.indices) {

            if (widgets[i].type == CodeSnippet.ConstantValues.WIDGET_EDIT_TEXT) {
                val editTextEmail = EditText(context)
                editTextEmail.hint = widgets[i].hint
                editTextEmail.tag = widgets[i].id
                //let's define params
                val params = defineParams(widgets[i].width, widgets[i].height)
                editTextEmail.layoutParams = params
                //gravity
                params.gravity = findGravity(widgets[i].gravity)
                val inputMethod = defineInputMethods(widgets[i].input_type)
                editTextEmail.inputType = inputMethod
                editTextEmail.textSize = widgets[i].text_size!!.toFloat()
                editTextEmail.isAllCaps = widgets[i].isAllCaps
                definedLayout.addView(editTextEmail)
            } else if (widgets[i].type == CodeSnippet.ConstantValues.WIDGET_BUTTON) {
                val buttonLogin = Button(context)
                buttonLogin.text = widgets[i].text
                buttonLogin.tag = widgets[i].id
                val params = defineParams(widgets[i].width, widgets[i].height)
                buttonLogin.layoutParams = params
                params.gravity = findGravity(widgets[i].gravity)

                buttonLogin.textSize = widgets[i].text_size!!.toFloat()
                buttonLogin.isAllCaps = widgets[i].isAllCaps
                definedLayout.addView(buttonLogin)
            }
        }
    }

    private fun defineParams(width: Int?, height: Int?): LinearLayout.LayoutParams {
        //let's define params
        val wid: Int = if (width == CodeSnippet.ConstantValues.VIEW_WIDTH_HEIGHT)
            LinearLayout.LayoutParams.MATCH_PARENT
        else
            LinearLayout.LayoutParams.WRAP_CONTENT

        val hei: Int = if (height == CodeSnippet.ConstantValues.VIEW_WIDTH_HEIGHT)
            LinearLayout.LayoutParams.MATCH_PARENT
        else
            LinearLayout.LayoutParams.WRAP_CONTENT

        return LinearLayout.LayoutParams(wid, hei)
    }

    private fun defineOrientation(orientation: Int?): Int {
        return if (orientation == CodeSnippet.ConstantValues.VIEW_ORIENTATION)
            LinearLayout.VERTICAL
        else
            LinearLayout.HORIZONTAL
    }

    private fun findGravity(gravity: Int?): Int {
        var finalGravity = -1
        when (gravity) {
            CodeSnippet.ConstantValues.GRAVITY_LEFT -> {
                finalGravity = Gravity.START
            }
            CodeSnippet.ConstantValues.GRAVITY_TOP -> {
                finalGravity = Gravity.TOP
            }
            CodeSnippet.ConstantValues.GRAVITY_RIGHT -> {
                finalGravity = Gravity.END
            }
            CodeSnippet.ConstantValues.GRAVITY_BOTTOM -> {
                finalGravity = Gravity.BOTTOM
            }
            CodeSnippet.ConstantValues.GRAVITY_CENTER -> {
                finalGravity = Gravity.CENTER
            }
        }
        return finalGravity
    }

    private fun defineInputMethods(inputType: Int): Int {
        var finalInputMethod = -1
        when (inputType) {
            CodeSnippet.ConstantValues.INPUT_EMAIL -> {
                finalInputMethod = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            }
            CodeSnippet.ConstantValues.INPUT_PASSWORD -> {
                finalInputMethod =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
            CodeSnippet.ConstantValues.INPUT_TEXT -> {
                finalInputMethod =
                    InputType.TYPE_CLASS_TEXT
            }
        }
        return finalInputMethod
    }
}