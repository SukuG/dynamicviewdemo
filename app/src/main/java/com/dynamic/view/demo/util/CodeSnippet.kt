package com.dynamic.view.demo.util

import android.content.Context
import java.io.IOException

class CodeSnippet {

    companion object {

        fun loadJSONFromAsset(context: Context, filename: String): String? {
            var json: String? = null
            try {
                val inputStream = context.assets.open(filename)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer, Charsets.UTF_8)
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }
            return json
        }
    }

    object ConstantValues {

        //Note: Below all values are need to define in backend.


        // 1 -> linear layout// 2 -> relative layout
        const val LAYOUT_LINEAR = 1
        const val LAYOUT_RELATIVE = 2

        // 1 -> match_parent//2 -> wrap_content//3 -> fill_parent
        const val VIEW_WIDTH_HEIGHT = 1

        // 1 -> vertical// 2  -> horizontal
        const val VIEW_ORIENTATION = 1

        // 1 -> edit_text//  2 -> button // 3 -> text_view
        const val WIDGET_EDIT_TEXT = 1
        const val WIDGET_BUTTON = 2
        const val WIDGET_TEXT_VIEW = 3

        //1->type_email // 2 -> type_password // 3 -> text // 4 -> phone number
        const val INPUT_EMAIL = 1
        const val INPUT_PASSWORD = 2
        const val INPUT_TEXT= 3

        // 1 -> Left// 2 -> Top// 3 -> Right// 4 -> Bottom// 5 -> Center
        const val GRAVITY_LEFT = 1
        const val GRAVITY_TOP = 2
        const val GRAVITY_RIGHT = 3
        const val GRAVITY_BOTTOM = 4
        const val GRAVITY_CENTER = 5
    }

}