package com.dynamic.view.demo.view

import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.dynamic.view.demo.IContracts.IMainActivityPresenter
import com.dynamic.view.demo.IContracts.IMainActivityView
import com.dynamic.view.demo.R
import com.dynamic.view.demo.presenter.MainActivityPresenter


class MainActivity : AppCompatActivity(), IMainActivityView {

    var iMainActivityPresenter: IMainActivityPresenter? = null
    var rootLayout: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        iMainActivityPresenter = MainActivityPresenter(this)
        //views
        rootLayout = findViewById(R.id.login_root)
        iMainActivityPresenter?.loadLoginViewResponse(this)
    }

    override fun setViewToParent(definedLayout: LinearLayout) {
        rootLayout?.addView(definedLayout)
    }
}
